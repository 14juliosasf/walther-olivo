
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingresa la cantidad de mov:");
        int n = scanner.nextInt();

        int[] mov = new int[n];
        System.out.println("Ingrese los mov (horizontal y vertical):");
        for (int i = 0; i < n; i++) {
            mov[i] = scanner.nextInt();
        }

        moverX(mov);
    }

    public static void moverX(int[] mov) {
        // Inicializar  posición de X
        int x = 0, y = 0;

        // Procesar los mov
        for (int i = 0; i < mov.length - 1; i += 2) {
            // Calcular la nueva posición
            x += mov[i];
            y += mov[i+1];

            // Asegurarse de que la X no salga del tablero
            x = Math.max(0, Math.min(x, 3));
            y = Math.max(0, Math.min(y, 3));
        }

        // Inicializar el tablero
        char[][] tablero = new char[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tablero[i][j] = 'O';
            }
        }
        tablero[y][x] = 'X';  //   y es la fila y x es la columna

        // Imprimir el tablero  
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(tablero[i][j]);
            }
            System.out.println();
        }
    }
}