import java.util.Scanner;

public class Main
{
	public static void main(String[] args) {
		algoritmo1();
	}

    public static void algoritmo1() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de elementos del arreglo: ");
        int N = obtenerNumeroEntero(scanner);

        int[] myArray = new int[N];

        System.out.println("Ingrese los elementos del arreglo (en el rango de 1 al 9):");
        for (int i = 0; i < N; i++) {
            System.out.print("Elemento " + (i + 1) + ": ");
            int numero = obtenerNumeroEntero(scanner);
            if (numero >= 1 && numero <= 9) {
                myArray[i] = numero;
            } else {
                System.out.println("El número debe estar en el rango de 1 al 9. Vuelva a ingresar.");
                i--; // Retroceder un índice para volver a pedir el número
            }
        }

        // Contar la frecuencia de los números
        Map<Integer, Integer> frecuencia = new HashMap<>();
        for (int num : myArray) {
            frecuencia.put(num, frecuencia.getOrDefault(num, 0) + 1);
        }

        // Encontrar el número más repetido y su frecuencia
        int maxFrecuencia = 0;
        int numeroRepetido = 0;
        for (Map.Entry<Integer, Integer> entry : frecuencia.entrySet()) {
            int numero = entry.getKey();
            int freq = entry.getValue();
            if (freq > maxFrecuencia) {
                maxFrecuencia = freq;
                numeroRepetido = numero;
            }
        }

        System.out.println("El número que más se repite es: " + numeroRepetido);
        System.out.println("Se repite " + maxFrecuencia + " veces.");

        scanner.close(); // Cerrar el scanner al finalizar su uso
    }

    public static int obtenerNumeroEntero(Scanner scanner) {
        int numero;
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("Debe ingresar un número entero.");
                scanner.nextLine(); // Limpiar el valor no válido del scanner
            }
            numero = scanner.nextInt();
            scanner.nextLine(); // Limpiar el salto de línea restante
        } while (numero < 1 || numero > 9);
        return numero;
    }
}
