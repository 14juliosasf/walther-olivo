CREATE DATABASE "pruebaDeTelconet"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Ecuador.1252'
    LC_CTYPE = 'Spanish_Ecuador.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;
	
--Creación de Tablas
CREATE TABLE admi_usuario (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) UNIQUE NOT NULL,
  password VARCHAR(100) NOT NULL,
  firstname VARCHAR(50) NOT NULL,
  lastname VARCHAR(50) NOT NULL,
  direccion VARCHAR(100) NOT NULL,
  telefono VARCHAR(20) NOT NULL,

  usuario_creacion VARCHAR(50),
  fecha_creacion TIMESTAMP,
  usuario_modificacion VARCHAR(50),
  fecha_modificacion TIMESTAMP,
  foto VARCHAR(200),
  status CHAR(1)
);
CREATE TABLE admi_rol (
  id SERIAL PRIMARY KEY,
  nombre VARCHAR(20) NOT NULL,
  descripcion VARCHAR(100)
);
CREATE TABLE info_usuario_rol (
  id SERIAL PRIMARY KEY,
  id_user INTEGER NOT NULL,
  id_role INTEGER NOT NULL,
  usuario_creacion VARCHAR(50),
  fecha_creacion TIMESTAMP,
  usuario_modificacion VARCHAR(50),
  fecha_modificacion TIMESTAMP,
  estado CHAR(20),
  FOREIGN KEY (id_user) REFERENCES users (id),
  FOREIGN KEY (id_role) REFERENCES roles (id)
);
