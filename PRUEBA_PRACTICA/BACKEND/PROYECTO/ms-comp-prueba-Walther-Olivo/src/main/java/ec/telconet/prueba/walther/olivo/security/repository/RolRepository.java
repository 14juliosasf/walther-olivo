package ec.telconet.prueba.walther.olivo.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
 import org.springframework.stereotype.Repository;
 
import ec.telconet.prueba.walther.olivo.security.entity.Rol;
 import ec.telconet.prueba.walther.olivo.security.enums.RolNombre;

 import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {
  
  
    Optional<Rol> findByRolNombre(RolNombre rolNombre);

 

    boolean existsByRolNombre(String nombreRol);

 
   //  public List<Rol> getAllRoleRols();


  
}
