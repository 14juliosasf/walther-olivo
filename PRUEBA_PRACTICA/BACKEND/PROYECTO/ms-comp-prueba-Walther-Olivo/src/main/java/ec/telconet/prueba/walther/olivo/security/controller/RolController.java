package ec.telconet.prueba.walther.olivo.security.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.walther.olivo.security.entity.Rol;
import ec.telconet.prueba.walther.olivo.security.service.RolService;

@RestController
@RequestMapping("/roles")
@CrossOrigin("http://localhost:4200")
public class RolController {

  @Autowired
  RolService rolService;

  @GetMapping("/listar")
  public ResponseEntity<List<Rol>> listarRoles() {
    return ResponseEntity.ok(rolService.getAll());
  }

  @GetMapping("/detalle/{id}")
  public ResponseEntity<Rol> getRolById(@PathVariable Integer id) {
    Optional<Rol> rolOptional = rolService.getById(id);
    if (rolOptional.isPresent()) {
      return ResponseEntity.ok(rolOptional.get());
    } else {
      return ResponseEntity.notFound().build();
    }
  }

 // @PreAuthorize("hasRole('ADMINISTRADOR')")
  @PostMapping("/crear")
  public ResponseEntity<Void> crearRol(@Valid @RequestBody Rol rol) {
    rolService.save(rol);
    return ResponseEntity.ok().build();
  }

//  @PreAuthorize("hasRole('ADMINISTRADOR')")
  @PutMapping("/actualizar/{id}")
  public ResponseEntity<Void> actualizarRol(@Valid @RequestBody Rol rol, @PathVariable Integer id) {
    Optional<Rol> rolOptional = rolService.getById(id);
    if (rolOptional.isPresent()) {
      Rol rolExistente = rolOptional.get();
      rolExistente.setRolNombre(rol.getRolNombre());
      rolExistente.setDescripcion(rol.getDescripcion());
      rolService.save(rolExistente);
      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.notFound().build();
    }
  }

//  @PreAuthorize("hasRole('ADMINISTRADORISTRADOR')")
  @DeleteMapping("/eliminar/{id}")
  public ResponseEntity<?> eliminarRol(@PathVariable Integer id) {
    Optional<Rol> rolOptional = rolService.getById(id);
    if (rolOptional.isPresent()) {
      rolService.deleteById(id);
      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.notFound().build();
    }
  }
}
