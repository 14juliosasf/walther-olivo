package ec.telconet.prueba.walther.olivo;

 
 import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 

 

@SpringBootApplication
public class OlimacServicesApplication {

	// @Autowired
	// UsuarioService usuarioService;

	public static void main(String[] args) {
		SpringApplication.run(OlimacServicesApplication.class, args);
	}

	// @Override
	// public void run(String... args) throws Exception {
	// 	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	// 	String contrasenaEncriptada = passwordEncoder.encode("2832938_Kk");
	// 	Rol rol = new Rol(RolNombre.ADMINISTRADOR);
	// 	Set<Rol> roles = new HashSet<>();
	// 	roles.add(rol);

	// 	Usuario usuario = new Usuario(1, "administrador", null, "@prueba.com", "administrador@prueba.com",
	// 			contrasenaEncriptada, "A", null, null, null, null, roles);

	// 	usuarioService.save(usuario);
	// }
}
