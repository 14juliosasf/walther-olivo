package ec.telconet.prueba.walther.olivo.security.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.sql.Date;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "admi_usuario")
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "El campo nombre no puede ser nulo")
    @NotBlank(message = "El campo nombre no puede estar vacio")
    @Column(name = "nombres", nullable = false)
    private String nombre;

    @NotNull(message = "El campo nombre no puede ser nulo")
    @NotBlank(message = "El campo nombre no puede estar vacio")
    @Column(name = "direccion", nullable = false)
    private String direccion;

    @NotNull(message = "El campo apellido no puede ser nulo")
    @NotBlank(message = "El campo apellido no puede estar vacio")
    @Size(max = 255, message = "El campo apellido debe tener un máximo de 255 caracteres")
    @Column(name = "apellidos", nullable = false)
    private String apellido;

    @NotNull(message = "El campo nombre de usuario no puede ser nulo")
    @Column(unique = true, name = "usuario")
    @Email(message = "El campo nombre de usuario debe ser un correo electrónico válido")
    private String nombreUsuario;
    
    @NotNull(message = "El campo password no puede ser nulo")
    @NotBlank(message = "El campo password no puede estar vacío")
     @Pattern(regexp = "^(?=.*[A-Z])(?=.*\\W).*$", message = "El campo password debe contener al menos una mayúscula y un carácter especial")
    @Column(name = "password")
    private String password;

    @NotBlank(message = "El campo estado no puede estar vacio")
    @Size(max = 1, message = "El campo estado debe tener un máximo de 1 caracteres")
    @Column(name = "estado")
    @Pattern(regexp = "[AI]", message = "El campo estado solo puede tener los valores: A Activo, I Inactivo")
    private String estado;

    @NotBlank
  @Pattern(regexp = "^\\d*$", message = "Solo puede contener números.")
  @Column(length = 20)
  private String telefono;

 //   @NotBlank(message = "El campo estado no puede estar vacio")
    private String usuario_creacion;

   // @NotBlank(message = "El campo estado no puede estar vacio")
    private Date fecha_creacion;

    @NotBlank(message = "El campo estado no puede estar vacio")
    private String foto;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "info_usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles = new HashSet<>();
    

     

    public Usuario(
            @NotNull(message = "El campo nombre no puede ser nulo") @NotBlank(message = "El campo nombre no puede estar vacio") String nombre,
            @NotNull(message = "El campo nombre no puede ser nulo") @NotBlank(message = "El campo nombre no puede estar vacio") String direccion,
            @NotNull(message = "El campo apellido no puede ser nulo") @NotBlank(message = "El campo apellido no puede estar vacio") @Size(max = 255, message = "El campo apellido debe tener un máximo de 255 caracteres") String apellido,
            @NotNull(message = "El campo nombre de usuario no puede ser nulo") @Email(message = "El campo nombre de usuario debe ser un correo electrónico válido") String nombreUsuario,
            @NotNull(message = "El campo password no puede ser nulo") @NotBlank(message = "El campo password no puede estar vacío") @Size(max = 8, message = "El campo password debe tener un máximo de 8 caracteres") @Pattern(regexp = "^(?=.*[A-Z])(?=.*\\W).*$", message = "El campo password debe contener al menos una mayúscula y un carácter especial") String password,
            @NotBlank(message = "El campo estado no puede estar vacio") @Size(max = 1, message = "El campo estado debe tener un máximo de 1 caracteres") @Pattern(regexp = "[AI]", message = "El campo estado solo puede tener los valores: A Activo, I Inactivo") String estado,
            @NotBlank @Pattern(regexp = "^\\d*$", message = "Solo puede contener números.") String telefono,
            String usuario_creacion, Date fecha_creacion,
            @NotBlank(message = "El campo estado no puede estar vacio") String foto) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.apellido = apellido;
        this.nombreUsuario = nombreUsuario;
        this.password = password;
        this.estado = estado;
        this.telefono = telefono;
        this.usuario_creacion = usuario_creacion;
        this.fecha_creacion = fecha_creacion;
        this.foto = foto;
    }

    

}
