package ec.telconet.prueba.walther.olivo.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import ec.telconet.prueba.walther.olivo.dto.Mensaje;
import ec.telconet.prueba.walther.olivo.security.dto.JwtDto;
import ec.telconet.prueba.walther.olivo.security.dto.LoginUsuario;
import ec.telconet.prueba.walther.olivo.security.dto.NuevoUsuario;
import ec.telconet.prueba.walther.olivo.security.entity.Rol;
import ec.telconet.prueba.walther.olivo.security.entity.Usuario;
import ec.telconet.prueba.walther.olivo.security.enums.RolNombre;
import ec.telconet.prueba.walther.olivo.security.jwt.JwtProvider;
import ec.telconet.prueba.walther.olivo.security.service.RolService;
import ec.telconet.prueba.walther.olivo.security.service.UsuarioService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@CrossOrigin( "http://localhost:4200")
public class AuthController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    RolService rolService;

    @Autowired
    JwtProvider jwtProvider;

    // @PostMapping("/nuevo")
    // public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
     
    //     if (bindingResult.hasErrors()) {
    //         return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
    //     }
        
    //     if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario())) {
    //         return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
    //     }
        
    //     if (usuarioService.existsByEmail(nuevoUsuario.getEmail())) {
    //         return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);
    //     }
        
    //     Usuario usuario = new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getApellido(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
    //             passwordEncoder.encode(nuevoUsuario.getPassword()));
        
    //     Set<Rol> roles = new HashSet<>();
    //     if (nuevoUsuario.getRoles().contains("ROLE_ADMIN")) {
    //         roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
    //     } else {
    //         roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
    //     }
    //     usuario.setRoles(roles);
    //     usuarioService.save(usuario);
        
    //     return new ResponseEntity(new Mensaje("usuario guardado"), HttpStatus.CREATED);
    // }

    // }
    
    @PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult) {
        
        if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario())) {
            return new ResponseEntity(new Mensaje("Ese nombre de usuario ya existe"), HttpStatus.BAD_REQUEST);
        }
        
        
        
          Usuario usuario = new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getDireccion(), nuevoUsuario.getApellido(), nuevoUsuario.getNombreUsuario(),  
           passwordEncoder.encode(nuevoUsuario.getPassword()), nuevoUsuario.getEstado(), nuevoUsuario.getTelefono(), nuevoUsuario.getUsuario_creacion(), (@NotBlank(message = "El campo estado no puede estar vacio") Date) nuevoUsuario.getFecha_creacion(), nuevoUsuario.getFoto());
        
        Set<Rol> roles = new HashSet<>();
     
        
        if (nuevoUsuario.getRoles().contains("ADMINISTRADOR")) {
            Optional<Rol> rolAdminOptional = rolService.getByRolNombre(RolNombre.ADMINISTRADOR);
            if (rolAdminOptional.isPresent()) {
                roles.add(rolAdminOptional.get());
            } else {
                return new ResponseEntity(new Mensaje("El rol de administrador no está configurado"), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else if (nuevoUsuario.getRoles().contains(RolNombre.CONSULTOR)) {
            Optional<Rol> rolConsultorOptional = rolService.getByRolNombre(RolNombre.CONSULTOR);
            if (rolConsultorOptional.isPresent()) {
                roles.add(rolConsultorOptional.get());
            } else {
                return new ResponseEntity(new Mensaje("El rol de consultor no está configurado"), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else if (nuevoUsuario.getRoles().contains(RolNombre.EDITOR)) {
            Optional<Rol> rolEditorOptional = rolService.getByRolNombre(RolNombre.EDITOR);
            if (rolEditorOptional.isPresent()) {
                roles.add(rolEditorOptional.get());
            } else {
                return new ResponseEntity(new Mensaje("El rol de editor no está configurado"), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity(new Mensaje("Los roles proporcionados no son válidos"), HttpStatus.BAD_REQUEST);
        }
        usuario.setRoles(roles);
        usuarioService.save(usuario);
        
        return new ResponseEntity(new Mensaje("Usuario guardado"), HttpStatus.CREATED);
    }
    
    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos"), HttpStatus.BAD_REQUEST);
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
    
        JwtDto jwtDto = new JwtDto(jwt);
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }


    @GetMapping("/usuarios")
    private List<Usuario> getAllUsers() {
      return usuarioService.getAll();
    }

    @GetMapping("/roles")
    private List<Rol> getAllRoles() {
      return rolService.getAll();
    }
}
