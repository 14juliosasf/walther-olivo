package ec.telconet.prueba.walther.olivo.security.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ec.telconet.prueba.walther.olivo.security.enums.RolNombre;
import lombok.AllArgsConstructor;
import lombok.Data;
 
@Data
@Entity
@Table(name = "admi_rol")
@AllArgsConstructor
public class Rol {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RolNombre rolNombre;


    @NotNull(message = "El campo descripcion no puede ser nulo")
    @NotBlank(message = "El campo descripcion no puede estar vacio")
    @Size(max = 255, message = "El campo descripcion debe tener un máximo de 255 caracteres")
    @Column(name = "descripcion", nullable = false)
    private String descripcion;


    public Rol() {
    }

    public Rol(@NotNull RolNombre rolNombre) {
        this.rolNombre = rolNombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RolNombre getRolNombre() {
        return rolNombre;
    }

    public void setRolNombre(RolNombre rolNombre) {
        this.rolNombre = rolNombre;
    }

}

