package ec.telconet.prueba.walther.olivo.security.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "info_usuario_rol")
public class InfoUsuarioRol {


    @Id
    
  @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

  @ManyToOne
  @JoinColumn(name = "id_Usuario")
  private Usuario Usuario;

  @ManyToOne
  @JoinColumn(name = "id_Rol")
  private Rol Rol;

  @Column(length = 50)
  private String usuario_creacion;

  @Column
  private Date fecha_creacion;

  @Column(length = 50)
  private String usuario_modificacion;

  @Column
  private Date fecha_modificacion;

  @Column(length = 20)
  private String estado;
  

  
}
