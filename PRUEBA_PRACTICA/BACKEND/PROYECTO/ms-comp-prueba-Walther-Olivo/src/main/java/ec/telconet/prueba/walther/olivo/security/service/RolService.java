package ec.telconet.prueba.walther.olivo.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.telconet.prueba.walther.olivo.security.entity.Rol;
import ec.telconet.prueba.walther.olivo.security.enums.RolNombre;
import ec.telconet.prueba.walther.olivo.security.repository.RolRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RolService {

    @Autowired
    RolRepository rolRepository;

    public Optional<Rol> getByRolNombre(RolNombre rolNombre){
        return rolRepository.findByRolNombre(rolNombre);
    }

    public void save(Rol rol){
        rolRepository.save(rol);
    }

    public Optional<Rol> getById(int id) {
        return rolRepository.findById(id);
    }

    public List<Rol> getAll(){
        return rolRepository.findAll();
    }

    

    

    public void deleteById(int id) {
        rolRepository.deleteById(id);
    }
}

