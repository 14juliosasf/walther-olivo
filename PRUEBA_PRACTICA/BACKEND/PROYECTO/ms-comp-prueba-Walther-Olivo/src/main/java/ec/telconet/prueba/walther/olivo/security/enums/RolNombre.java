package ec.telconet.prueba.walther.olivo.security.enums;

public enum RolNombre {
    ADMINISTRADOR, EDITOR, CONSULTOR, CLIENTE
}
