package ec.telconet.prueba.walther.olivo.security.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NuevoUsuario {
    @NotBlank
    private String nombre;

    @NotBlank
    private String apellido;

    @NotBlank
    private String nombreUsuario;

  @NotBlank
    private String usuario_creacion;

    @NotBlank
    private String foto;

     private String telefono;

    //@NotBlank
    private Date fecha_creacion;

    @NotBlank
    private String estado;

     @NotBlank
    private String direccion;
    @Email
    private String email;

    @NotBlank
    private String password;

    private Set<String> roles = new HashSet<>();

}
