import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesRolComponent } from './detalles-rol.component';

describe('DetallesRolComponent', () => {
  let component: DetallesRolComponent;
  let fixture: ComponentFixture<DetallesRolComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetallesRolComponent]
    });
    fixture = TestBed.createComponent(DetallesRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
