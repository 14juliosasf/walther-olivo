import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Rol } from 'src/app/models/rol';
import { RolesService } from 'src/app/services/roles/roles.service';
import { TokenService } from 'src/app/services/token/token.service';

@Component({
  selector: 'app-listar-roles',
  templateUrl: './listar-roles.component.html',
  styleUrls: ['./listar-roles.component.scss']
})
export class ListarRolesComponent {
  roles: any;
  isAdmin = false;
  deleteRol:  Rol = {
    
    id: 0,
    RolNombre: '',
   
    
    descripcion: '',
   
    }

  constructor(
    private rolService: RolesService,
    private toastr: ToastrService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.cargarProductos();
    this.isAdmin = this.tokenService.isAdmin();
  }

  cargarProductos(): void {
    this.rolService.lista().subscribe(
      data => {        
        this.roles = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  
  borrar(id: number) {
    this.rolService.deleteRol(id).subscribe(
      data => {
        this.toastr.success('Producto Eliminado', 'OK', {
          timeOut: 2000, positionClass: 'toast-top-center'
        });
        this.cargarProductos();
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 2000, positionClass: 'toast-top-center',
        });
      }
    );
  }
  

}
