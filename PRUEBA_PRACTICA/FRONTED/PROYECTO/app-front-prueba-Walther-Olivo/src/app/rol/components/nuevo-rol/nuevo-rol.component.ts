import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Rol } from 'src/app/models/rol';
import { RolesService } from 'src/app/services/roles/roles.service';

@Component({
  selector: 'app-nuevo-rol',
  templateUrl: './nuevo-rol.component.html',
  styleUrls: ['./nuevo-rol.component.scss']
})
export class NuevoRolComponent {

  RolNombre = '';
   
  descripcion = '';

constructor(
  private rolService: RolesService,
  private toastr: ToastrService,
  private router: Router
  ) { }

ngOnInit() {
}

onCreate(): void {
  const rol = new Rol(this.RolNombre, this.descripcion);
  this.rolService.createRol(rol).subscribe(
    data => {
      this.toastr.success('Producto Creado', 'OK', {
        timeOut: 2000, positionClass: 'toast-top-center'
      });
      this.router.navigate(['/listar']);
    },
    err => {
      this.toastr.error(err.error.mensaje, 'Fail', {
        timeOut: 2000,  positionClass: 'toast-top-center',
      });
    }
  );
}

}
