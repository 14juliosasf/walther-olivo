import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RolesService } from 'src/app/services/roles/roles.service';

@Component({
  selector: 'app-editar-rol',
  templateUrl: './editar-rol.component.html',
  styleUrls: ['./editar-rol.component.scss']
})
export class EditarRolComponent {

  rol: any;

  constructor(
    private rolService: RolesService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    this.rolService.getRolById(id).subscribe(
      data => {
        this.rol = data;
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 2000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    this.rolService.updateRol(id, this.rol).subscribe(
      data => {
        this.toastr.success('Producto Actualizado', 'OK', {
          timeOut: 2000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/listar']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 2000,  positionClass: 'toast-top-center',
        });
     
      }
    );
  }

}

