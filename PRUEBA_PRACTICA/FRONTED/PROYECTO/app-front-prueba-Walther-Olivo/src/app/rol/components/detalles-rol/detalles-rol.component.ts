import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RolesService } from 'src/app/services/roles/roles.service';

@Component({
  selector: 'app-detalles-rol',
  templateUrl: './detalles-rol.component.html',
  styleUrls: ['./detalles-rol.component.scss']
})
export class DetallesRolComponent {

  rol: any;

  constructor(
    private rolsService: RolesService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    this.rolsService.getRolById(id).subscribe(
      data => {
        this.rol = data;
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 2000,  positionClass: 'toast-top-center',
        });
        this.volver();
      }
    );
  }

  volver(): void {
    this.router.navigate(['/listar']);
  }

}
