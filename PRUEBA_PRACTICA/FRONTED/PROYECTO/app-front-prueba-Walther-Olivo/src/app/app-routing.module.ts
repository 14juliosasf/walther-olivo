import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './auth/components/login/login.component';
import { RegistroComponent } from './auth/components/registro/registro.component';
 import { ProdGuardService as guard } from './services/guards/prod-guard.service';
   import { ListaUsuariosComponent } from './usuarios/components/lista-usuarios/lista-usuarios.component';
import { DetalleUsuariosComponent } from './usuarios/components/detalle-usuarios/detalle-usuarios.component';
import { EditarUsuariosComponent } from './usuarios/components/editar-usuarios/editar-usuarios.component';
import { NuevoUsuarioComponent } from './usuarios/components/nuevo-usuario/nuevo-usuario.component';
import { ListarRolesComponent } from './rol/components/listar-roles/listar-roles.component';
import { EditarRolComponent } from './rol/components/editar-rol/editar-rol.component';
import { NuevoRolComponent } from './rol/components/nuevo-rol/nuevo-rol.component';
import { DetallesRolComponent } from './rol/components/detalles-rol/detalles-rol.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
    { path: 'listar-usuarios', component: ListaUsuariosComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR', 'CONSULTOR'] } },
  { path: 'detalleUser/:id', component: DetalleUsuariosComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR', 'CONSULTOR'] } },
  { path: 'editarUser/:id', component: EditarUsuariosComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR'] } },

  { path: 'nuevoUser', component: NuevoUsuarioComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR'] } },


  { path: 'listar', component: ListarRolesComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR', 'CONSULTOR'] } },
  { path: 'editarRol/:id', component: EditarRolComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR'] } },
  { path: 'nuevoRol', component: NuevoRolComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR'] } },
  { path: 'detalleRol/:id', component: DetallesRolComponent, canActivate: [guard], data: { expectedRol: ['ADMINISTRADOR', 'CONSULTOR'] } },

  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
