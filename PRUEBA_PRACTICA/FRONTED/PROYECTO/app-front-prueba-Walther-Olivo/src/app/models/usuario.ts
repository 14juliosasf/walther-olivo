export class Usuario {
    nombre: string;
    apellido: string;
    nombreUsuario: string;
    estado: string;
    direccion: string;
    telefono: string;
     password: string;
    constructor(nombre: string, apellido: string, nombreUsuario: string, estado: string, direccion: string , telefono: string, password: string) {
        this.nombre = nombre;
        this.apellido = apellido
        this.nombreUsuario = nombreUsuario;
        this.estado = 'Activo'
        this.direccion = direccion;
        this.telefono = telefono;
         this.password = password;
    }
}
