import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rol } from 'src/app/models/rol';
import { environment } from 'src/assets/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  rolesURL = environment.rolesURL;

  constructor(private httpClient: HttpClient) { }


  public lista(): Observable<Rol[]> {
    return this.httpClient.get<Rol[]>(this.rolesURL + 'listar');
  }

  createRol(Rol:Rol): Observable<Object>{
    return this.httpClient.post(this.rolesURL + 'crear', Rol );
  }

  deleteRol(id:number  ): Observable<Object>{
    return  this.httpClient.delete(this.rolesURL +  `eliminar/${id}`  );
    
  }

  getRolById(id:number): Observable<Rol>{
    return  this.httpClient.get<Rol>( this.rolesURL  +  `detalle/${id}`);
  }

  updateRol(id:number,  Rol:Rol): Observable<Object>{
    return  this.httpClient.put(this.rolesURL +  `actualizar/${id}`, Rol);
  }

}